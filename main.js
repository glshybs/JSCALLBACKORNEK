/*var veri=null;
function data(callback){
  console.log("data alınıyor");
  setTimeout(function () {
    veri="geldiiiii";
    callback();
  },1000);
}
function yazdir() {
  console.log("alındı",veri);
}
// veriyi alıyor ama beklemiyor 1000 ms sonra veri alınıyor beklemesi için şunu yapacağız
data(yazdir);
// birden fazla işlemde ise
*/
var veri=null;
function data(callback1,callback2) {
  console.log("data alınıyor");
  setTimeout(function () {
    veri="geldiiii";
    callback1(callback2);
  },1000);
}
function yazdirma(callback2) {
  setTimeout(function(){
    callback2();
  });
}
function konsoleyazdir() {
  console.log("data alındı",veri);
}
data(yazdirma,konsoleyazdir);
